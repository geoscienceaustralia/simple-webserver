# Packer Simple Webserver
> This will create an ami with apache2 installed
> This image uses the [GA Standard image](https://bitbucket.org/geoscienceaustralia/patch_agent) as a source image

## Packages
1. Apache2
2. Goss (for running tests)
3. ufw (firewall)

## Usage
1. Download and install Packer from [packer.io](http://packer.io)
2. `packer build build.json` 
3. Deploy a new EC2 instance using the AMI that was created in step 2.

## Customisations from default apache

### Network
* Configure a software firewall
* Secure shared memory (from source image)
* IP Spoofing protection (from source image)
* Ignore ICMP broadcast requests (from source image)
* Disable source packet routing (from source image)
* Ignore send redirects (from source image)
* Block SYN Attacks (from source image)
* Log Martians (from source image)
* Ignore ICMP Redirects (from source image)
* Ignore Directed pings (from source image)

### Apache
* Hide server signatures and headers
* Turn TraceEnable Off

#### Pipeline Dependencies
* For the pipeline to run and build successfully, it must pass tests defined in files/testDefinitions.sh
* The running of the tests relies on the test suite located in the [image-testing repo](https://bitbucket.org/geoscienceaustralia/image-testing/src/master/)
* To pull the test code, an SSH access key for this pipeline must be generated and added to the image-testing repo. View the image-testing readme for more details 

## Contributing
* Tests are defined in the files/goss file and use [goss](https://github.com/aelsabbahy/goss)
* If you have added any software please ensure it is reflected in the tests

## ROADMAP
### Add HTTPS Support
* Generate self signed cert (to sit behind elb)
* configure apache to work with ssl

### Improve Security logging
* Install PSAD
* Install RKHunter and CHKRootKit
* Install NMAP
* Setup LogWatch
* Setup Tiger and Tripwire
* Run all of these tools nightly
* Install logstash / fluentd
* send logs to monitoring server by default
* Add prometheus support
