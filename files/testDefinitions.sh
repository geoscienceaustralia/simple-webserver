
servicesToTest=snap.amazon-ssm-agent.amazon-ssm-agent,amazon-cloudwatch-agent,apache2,ufw
packagesToTest=python3,apache2
files=/etc/fstab,/etc/logrotate.d/apache2

python3 /tmp/image-testing/runtests.py --services $servicesToTest --packages $packagesToTest --files $files

exit $?