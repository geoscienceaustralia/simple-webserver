#!/bin/bash

sudo apt-get update

# Install apache
sudo apt-get install -y apache2
sudo apt-get install -y apache2-utils

# Install firewall
sudo apt-get install -y ufw
