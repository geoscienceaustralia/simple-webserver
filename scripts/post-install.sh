#!/bin/bash

echo ==============================================================================
echo Configure Firewall 
echo ==============================================================================

## Enable SSH access
sudo ufw allow ssh

## Enable HTTP Access
sudo ufw allow Apache

## Turn it on
sudo ufw --force enable

echo ==============================================================================
echo Configure Apache #
echo ==============================================================================

# add mod headers so we can disable ETags
pushd /etc/apache2/mods-enabled
sudo ln -s ../mods-available/headers.load headers.load
popd
# Configure Apache to not tell everyone what it is
sudo mv -f /tmp/files/security.conf /etc/apache2/conf-available/
# Add a default site
sudo mv /tmp/files/index.html /var/www/html/index.html
sudo service apache2 restart

# Configure logrotate
sudo mv -f /tmp/files/apache2 /etc/logrotate.d/apache2
sudo chown root:root /etc/logrotate.d/apache2 
sudo chmod 644 /etc/logrotate.d/apache2 

# Make it run hourly
sudo mv /etc/cron.daily/logrotate /etc/cron.hourly/